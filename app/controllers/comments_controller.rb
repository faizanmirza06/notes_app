class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]
  before_action :set_note, only: [:create, :show, :edit, :update, :destroy]

  def create

    @comment = @note.comments.build(comments_params)
    @comment.user = current_user
    @comment.save

    redirect_to notes_url
  end

  def edit
  end

  def show
  end

  def update

    respond_to do |format|
      if @comment.update(comments_params)
        format.html { redirect_to root_url, notice: 'Comment was successfully update.' }
      #format.jason { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit }
        format.jason { render jason: @comment.errors, status: :unprocessable_entity}
      end
    end
  end

  def destroy
    @note = Note.find(params[:note_id])
    @comment = @note.comments.find(params[:id]).destroy

    redirect_to notes_url
  end

  private

  def set_comment
    p "___________________________"
    p @comment
    @comment = Comment.find(params[:id])

  end
  def set_note
    @note = Note.find(params[:note_id])

  end

  def comments_params
    params.require(:comment).permit(:content)
  end
end
